#include <stdio.h>
#define X 100
#define xCalc (y * 3)
#define xCalc2(x) (x * 5)

union data{
	int id;
	char name;
	short num;
};

int main(){
	int result, result2, y;
	y = 10;
	result = X - xCalc;
	printf("%d", result);
	result2 = X - xCalc2(y);
	printf("%d\n\n", result2);

	union data a;
	printf("%d, %d\n", sizeof(a), sizeof(union data));
	a.id = 0x40;
	printf("%X, %c, %hX\n", a.id, a.name, a.num);
	a.name = 'A';
	printf("%X, %c, %hX\n", a.id, a.name, a.num);
	a.num = 0x2059;
	printf("%X, %c, %hX\n", a.id, a.name, a.num);
	a.id = 0x3E25AD54;
	printf("%X, %c, %hX\n", a.id, a.name, a.num);
	printf("%d, %d\n", sizeof(a), sizeof(union data));
	return 0;
}
